package cashEvaluacion.example.cashEvaluacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CashEvaluacionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CashEvaluacionApplication.class, args);
	}

}
